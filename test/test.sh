#!/usr/bin/env bash

##compile
g++ hand_gesture_monitor_demo.cpp -std=c++11 -O3 -L ../lib -lopencv_calib3d  -lopencv_core -lopencv_dnn -lopencv_features2d -lopencv_flann  -lopencv_highgui -lopencv_imgcodecs -lopencv_imgproc -lopencv_ml -lopencv_objdetect -lopencv_photo -lopencv_shape -lopencv_stitching -lopencv_superres -lopencv_videoio -lopencv_video -lopencv_videostab  -lcublas -lcudart -lcurand -lopenblas -lMomoCVCore  -I ../include

export LD_LIBRARY_PATH=../lib:$LD_LIBRARY_PATH
##test
./a.out ../resources
