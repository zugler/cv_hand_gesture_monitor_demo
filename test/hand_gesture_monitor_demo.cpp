//##include <MomoCVCore/face_shake_monitor/face_shake_monitor.h>
#include <string>
#include <vector>
#include <opencv2/opencv.hpp>
#include <fstream>
#include <ostream>
#include <mutex>
#include <MomoCVCore/api_base/momocv_api.h>

#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

#include <iostream>
#include <ctype.h>
#include <cstdio>
#include <MomoCVCore/object_detect/object_detect.h>

#include <memory>
#include <core/MMCore.hpp>



int main(int argc, char* argv[])
{
    mmcv::ObjectDetect OD(0);
    std::string path = argv[1];

    if(! OD.load_model(path+"/10.20190806.153300.2.od-model"))
  // if(! OD.load_model(path+"/10.20181126.193259.2.od-model"))
    {
        printf("ObjectDetect load_model fail");
        return 0;
    }
    std::cout << "fy" << std::endl;
    //std::string video_path = "/Users/fuyu/Desktop/111.mov";
    //cv::VideoCapture cap(video_path);
    mmcv::Mat image;
    image = cv::imread(path+"/2.png");
    mmcv::MMFrame mmframe;
    mmcv::ObjectDetectParams base_params;
    mmcv::ObjectDetectInfo ret;
    int times = 0;
    while(true){
        
        if(image.empty())//如果某帧为空则退出循环
            break;
        times ++;
        if (times == 100000){
            break;
        }
        mmframe.data_ptr_ = image.data;
        mmframe.data_len_ = image.rows * image.step1();
        mmframe.format_ = mmcv::FMT_BGR;
        mmframe.width_ = image.getFrameCols();
        mmframe.height_ = image.getFrameRows();
        mmframe.step_ = image.step1();
        std::cout << image.step1() << std::endl;
        base_params.rotate_degree_ = 0;
        base_params.restore_degree_ = 0;
        base_params.fliped_show_ = 0;
        base_params.scale_factor_ = 1.0;
        
//        cv::imshow("a" , image);
//        cv::waitKey();

//        cv::resize(display, display, cv::Size(display.cols*ratio, display.rows*ratio));
        int64 t0 = cv::getTickCount();
        std::cout << "fuyu2" << std::endl;
        bool res = OD.process_frame(mmframe, base_params, ret);
        std::cout << (cv::getTickCount() - t0)/cv::getTickFrequency() * 1000 << "\t ms" <<  std::endl;
//        MMLOGD("process frame cost : %f ms",(cv::getTickCount() - t0)/cv::getTickFrequency() * 1000  );
        for(auto &s_ret : ret.detect_results_) {
            cv::rectangle(image, cv::Rect(s_ret.x_, s_ret.y_, s_ret.width_, s_ret.height_), cv::Scalar(255, 255, 0), 2);
            char tag[512];
            cv::putText(image, s_ret.class_name_  , cv::Point(s_ret.x_, s_ret.y_), 1, 1.5, cv::Scalar(0, 255, 255), 2);
        }
        cv::imwrite(path+"/2out.jpg", image);
        //cv::waitKey(10);
        
        
    }
    
}
