#ifndef _MM_PARAMS_H_
#define _MM_PARAMS_H_

#include <string>
#include <vector>
#include <opencv2/opencv.hpp>

namespace mmcv
{
    // 人脸姿态估计 ---------------------------
    struct PMObjectV2
    {
        /* pinhole camera model */
        /* 3x3 matrix */
        cv::Mat camera_matrix;
        /* 3x3 matrix */
        cv::Mat rotation_matrix;
        /* 1x3 vector or 3x1 vector*/
        cv::Mat eulers;
        /* 1x3 vector or 3x1 vector*/
        cv::Mat rotation_vector;
        /* 1x3 vector or 3x1 vector*/
        cv::Mat translation_vector;
        /* 3x4 matrix */
        cv::Mat projection_matrix;
        
        /* for opengl */
        /* 4x4 matrix */
        cv::Mat modelview_matrix;
        // original 3x4 projection doesn't consider z-order culling that an OpenGL system does.
        // so we need to transfer the original coordinate(image coordinate) to NDC coordinate
        // original coordinate transform:
        // [x y z].t() = R * [X Y Z].t() + t
        // x' = x / z
        // y' = y / z
        // u = fx * x' + cx
        // v = fy * y' + cy
        // u / image_width * 2 - 1 --> [-1 1]
        // v / image_height * 2 - 1 --> [-1 1]
        // float far = 100000, near = 0.01;
        // so opengl projection matrix equal:
        // [2 * fx / image_width                        0                                          -1 + (2 * cx / image_width)                     0                               ]
        // [0                                           2 * fy / image_height                      -1 + (2 * cy / image_height)                    0                               ]
        // [0                                           0                                          -(far + near) / (far - near)                    -2 * far * near / (far - near)  ]
        // [0                                           0                                          -1                                              0                               ]
        /* 4x4 matrix */
        cv::Mat projection_matrix_opengl;
    };
    
    // 人脸关键点 ---------------------------
    
    struct FaceAlignmentResult
    {
        FaceAlignmentResult() : trackingProbe(0), rotateDegree(0), trackingId(0) {}
        cv::Rect rect;
        std::vector<float> landmarks;
        std::vector<float> landmarks87;
        std::vector<float> eulerangels;
        std::vector<float> pointsOccProb;
        float trackingProbe;
        float rotateDegree;
        uint64_t trackingId; // to do. uint64_t in java is wrong.
    };
    
    struct FaceAlignmentParams
    {
        explicit FaceAlignmentParams(int degree = 0,
            bool tracking_flag = true, 
            int max_faces = 3, 
            bool use_lk_flag = true, 
            float stable_coef_ = 2, 
            bool save_small_faces_flag = false,
            bool use_npd = true) :
            rot_deg(degree),
            use_tracking(tracking_flag), 
            max_faces_num(max_faces), 
            use_lk(use_lk_flag), 
            stable_coef(stable_coef_),
            use_npd(use_npd){}

        float rot_deg;
        bool use_tracking;
        int max_faces_num;
        bool use_lk;
        float stable_coef;
        bool use_npd;
    };

    enum FORWARD_MODEL_TYPE
    {
        MODEL_TYPE_MMFORWARD = 1,
        MODEL_TYPE_MACE = 2,
        MODEL_TYPE_COREML = 4,
        MODEL_TYPE_SNPE = 8,
    };
}

#endif
