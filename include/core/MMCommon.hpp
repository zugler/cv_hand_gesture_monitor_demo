#ifndef MM_COMMON_H
#define MM_COMMON_H

#include <stdint.h>
#include <cfloat>
#include <climits>
#include <vector>
#include <string.h>
#include <string>
#include <cmath>
#include <assert.h>
#include <memory>

#ifdef _WIN32
//define something for Windows (32-bit and 64-bit, this part is common)
#define MOMO_WIN32_64
#ifdef _WIN64
//define something for Windows (64-bit only)
#define MOMO_WIN64
#endif
#elif __APPLE__
#include "TargetConditionals.h"
//#define USE_ACCELERATE //move to cmake config.
#if TARGET_IPHONE_SIMULATOR
// iOS Simulator
#define MOMO_IOS_SIMULATOR
#elif TARGET_OS_IPHONE
// iOS device
#define MOMO_IOS
#elif TARGET_OS_MAC
// Other kinds of Mac OS
#if !defined(MOMO_MAC)
#define MOMO_MAC
#endif
#else
#error "Unknown Apple platform"
#endif
#elif __ANDROID__
// Android
#if !defined(MOMO_ANDROID)
#define MOMO_ANDROID
#endif
//#define USE_OPENBLAS //recommended to use openblas
//#define USE_PERFBLAS
//#define USE_QBLAS
//#define USE_NNPACK
#elif __linux__
// linux
#define MOMO_LINUX
#elif __unix__ // all unices not caught above
// Unix
#define MOMO_UNIX
#elif defined(_POSIX_VERSION)
// POSIX
#define MOMO_POSIX
#else
#error "Unknown compiler"
#endif

#if defined _WIN32 || defined __CYGWIN__
#if defined(MMCV_EXPORTS) && defined(DLL_PROJECT)
#ifdef __GNUC__
#define MMCV_EXPORT __attribute__ ((dllexport))
#else
#define MMCV_EXPORT __declspec(dllexport) // Note: actually gcc seems to also supports this syntax.
#endif
#elif defined(DLL_PROJECT)
#ifdef __GNUC__
#define MMCV_EXPORT __attribute__ ((dllimport))
#else
#define MMCV_EXPORT __declspec(dllimport) // Note: actually gcc seems to also supports this syntax.
#endif
#else
#define MMCV_EXPORT
#endif
#define MMCV_HIDDEN
#else
#if __GNUC__ >= 4
#define MMCV_EXPORT
#define MMCV_HIDDEN __attribute__ ((visibility ("hidden")))
#else
#define MMCV_EXPORT
#define MMCV_HIDDEN
#endif
#endif

#ifdef __ANDROID__
#define MMCV_SOFTFP_CALLING  __attribute__((pcs("aapcs")))
#else
#define MMCV_SOFTFP_CALLING
#endif

//0b: 01-->16bit, 00-->32bit
#undef HALF_FLOAT
#undef FULL_FLOAT
#undef HALF_MASK
#undef VERSION_MASK
#define HALF_FLOAT (1 << 30)
#define FULL_FLOAT 0
#define HALF_MASK 0xF0000000
#define FORWARD_ENGINE_MASK 0x0F000000
#define VERSION_MASK 0x0000FFFF

// Disable the copy and assignment operator for a class.
#define MOMO_DISABLE_COPY_AND_ASSIGN(classname) \
private:\
	classname(const classname&);\
	classname& operator=(const classname&)

// Instantiate a class with float and double specifications.
#define MOMO_INSTANTIATE_CLASS(classname) \
    char gInstantiationGuard##classname; \
    template class MMCV_EXPORT classname<float>; \
    template class MMCV_EXPORT classname<double>

#include "MMLog.hpp"

#endif
