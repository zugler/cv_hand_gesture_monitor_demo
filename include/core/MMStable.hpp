// 2017-03-21
// zhang.shengkai@immomo.com
#pragma once
#include <fstream>
#include <vector>
#include <core/MMCore.hpp>

namespace mmcv
{
    class MMCV_EXPORT MMStable {
    public:
        MMStable(float threshold = 5, float alpha = 1.5);
        
    public:
        void Reset(const std::vector<float>& cur_vec);
        void Update(std::vector<float>& cur_vec);
        
        void Reset(const float* cur_vec, int vec_len);
        void Update(float* cur_vec, int vec_len);

        void UpdateSmooth(std::vector<float>& cur_vec);
        void UpdateSmooth(float* cur_vec, int vec_len);
        
        void SetThreshold(float threshold);
        void SetAlpha(int alpha);
        
    //protected:
        float threshold_;
        float alpha_;
        std::vector<float> pre_realtime_;
        std::vector<float> pre_stable_;
        
        inline float sigmoid(float x) {
            return (1. / (1. + std::exp(x)));
        }
        
    };
}
