#ifndef MM_LOG_H
#define MM_LOG_H

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <cmath>
#include <algorithm>

#define FILENAME_LENGTH 24

#define MMLOGLEVEL_DEBUG 4
#define MMLOGLEVEL_INFO 3
#define MMLOGLEVEL_WARNING 2
#define MMLOGLEVEL_ERROR 1

#define MMLOG_LEVEL MMLOGLEVEL_DEBUG

#if defined(NLOG)
#ifdef MMLOG_LEVEL
#undef MMLOG_LEVEL
#endif
#define MMLOG_LEVEL MMLOGLEVEL_ERROR
#endif

#if defined(__ANDROID__)
#include <cpu-features.h>
#include <android/log.h>
#define LOG_TAG "mmcv"

#if MMLOG_LEVEL >= MMLOGLEVEL_ERROR
#define MMLOGE(s, args...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "[E]%s(%d):" s "\n", __FILE__ + (strlen(__FILE__) - std::min(strlen(__FILE__), (size_t)FILENAME_LENGTH)), __LINE__, ##args)
#else
#define MMLOGE(s, args...)
#endif

#if MMLOG_LEVEL >= MMLOGLEVEL_WARNING
#define MMLOGW(s, args...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, "[W]%s(%d):" s "\n", __FILE__ + (strlen(__FILE__) - std::min(strlen(__FILE__), (size_t)FILENAME_LENGTH)), __LINE__, ##args)
#else
#define MMLOGW(s, args...)
#endif

#if MMLOG_LEVEL >= MMLOGLEVEL_INFO
#define MMLOGI(s, args...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, "[I]%s(%d):" s "\n", __FILE__ + (strlen(__FILE__) - std::min(strlen(__FILE__), (size_t)FILENAME_LENGTH)), __LINE__, ##args)
#else
#define MMLOGI(s, args...)
#endif

#if MMLOG_LEVEL >= MMLOGLEVEL_DEBUG
#define MMLOGD(s, args...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "[D]%s(%d):" s "\n", __FILE__ + (strlen(__FILE__) - std::min(strlen(__FILE__), (size_t)FILENAME_LENGTH)), __LINE__, ##args)
#else
#define MMLOGD(s, args...)
#endif

#else

#if MMLOG_LEVEL >= MMLOGLEVEL_ERROR
#if defined(BUILD_JNI)
#define MMLOGE(s, ...) printf("[E]%s(%d):" s "\n", __FILE__ + (strlen(__FILE__) - std::min(strlen(__FILE__), (size_t)FILENAME_LENGTH)), __LINE__, ##__VA_ARGS__);fflush(stdout)
#else
#define MMLOGE(s, ...) printf("[E]%s(%d):" s "\n", __FILE__ + (strlen(__FILE__) - std::min(strlen(__FILE__), (size_t)FILENAME_LENGTH)), __LINE__, ##__VA_ARGS__)
#endif
#else
#define MMLOGE(s, ...)
#endif

#if MMLOG_LEVEL >= MMLOGLEVEL_WARNING
#if defined(BUILD_JNI)
#define MMLOGW(s, ...) printf("[W]%s(%d):" s "\n", __FILE__ + (strlen(__FILE__) - std::min(strlen(__FILE__), (size_t)FILENAME_LENGTH)), __LINE__, ##__VA_ARGS__);fflush(stdout)
#else
#define MMLOGW(s, ...) printf("[W]%s(%d):" s "\n", __FILE__ + (strlen(__FILE__) - std::min(strlen(__FILE__), (size_t)FILENAME_LENGTH)), __LINE__, ##__VA_ARGS__)
#endif
#else
#define MMLOGW(s, ...)
#endif

#if MMLOG_LEVEL >= MMLOGLEVEL_INFO
#if defined(BUILD_JNI)
#define MMLOGI(s, ...) printf("[I]%s(%d):" s "\n", __FILE__ + (strlen(__FILE__) - std::min(strlen(__FILE__), (size_t)FILENAME_LENGTH)), __LINE__, ##__VA_ARGS__);fflush(stdout)
#else
#define MMLOGI(s, ...) printf("[I]%s(%d):" s "\n", __FILE__ + (strlen(__FILE__) - std::min(strlen(__FILE__), (size_t)FILENAME_LENGTH)), __LINE__, ##__VA_ARGS__)
#endif
#else
#define MMLOGI(s, ...)
#endif

#if MMLOG_LEVEL >= MMLOGLEVEL_DEBUG
#if defined(BUILD_JNI)
#define MMLOGD(s, ...) printf("[D]%s(%d):" s "\n", __FILE__ + (strlen(__FILE__) - std::min(strlen(__FILE__), (size_t)FILENAME_LENGTH)), __LINE__, ##__VA_ARGS__);fflush(stdout)
#else
#define MMLOGD(s, ...) printf("[D]%s(%d):" s "\n", __FILE__ + (strlen(__FILE__) - std::min(strlen(__FILE__), (size_t)FILENAME_LENGTH)), __LINE__, ##__VA_ARGS__)
#endif
#else
#define MMLOGD(s, ...)
#endif

#endif

#if defined(__ANDROID__)
#define MMLOG(s, args...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, "[SYS] " s "\n", ##args)
#else
#if defined(BUILD_JNI)
#define MMLOG(s, ...) printf("[SYS] " s "\n", ##__VA_ARGS__);fflush(stdout)
#else
#define MMLOG(s, ...) printf("[SYS] " s "\n", ##__VA_ARGS__)
#endif
#endif

#endif
