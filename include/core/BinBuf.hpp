//  songkey@outlook.com
//  2017-04-15

#ifndef BinBuf_hpp
#define BinBuf_hpp

#include <vector>
#include <fstream>
#include <stdint.h>
#include <core/MMCommon.hpp>

#define MODEL_POST_MAGIC_CODE 366996663

namespace mmcv
{
    MMCV_EXPORT bool check_file(const std::string& _name);

    template<typename Dtype>
    bool ConcatBuf(const std::vector<std::vector<Dtype> > bufs, std::vector<uint8_t>& dst_buf);

    MMCV_EXPORT bool  LoadTxtToString(const std::string& file_path, std::string& ret_str);
    
    template<typename Dtype>
    bool SplitBuf(const std::vector<uint8_t>& buf, std::vector< std::vector<Dtype> >& dst_bufs);
    
    MMCV_EXPORT bool LoadBinFile(const std::string& file_name, std::vector<uint8_t>& buf);
    MMCV_EXPORT  bool LoadBinFile(const std::string& file_name, std::vector<float>& buf);
    
    MMCV_EXPORT bool EncryptBuf_Fast(std::vector<uint8_t>& buf);
    MMCV_EXPORT bool DecryptBuf_Fast(std::vector<uint8_t>& buf);
    
    MMCV_EXPORT bool WriteBufToBin(const std::vector<uint8_t>& buf, const std::string& dst_path);
    MMCV_EXPORT bool WriteBufToTxt(const std::vector<uint8_t>& buf, const std::string& dst_path, const std::string& prefix = "model_buf");
    
    MMCV_EXPORT bool EncryptFileToVec(const std::string& src_file, std::vector<uint8_t>& _dst_ve);
    MMCV_EXPORT bool EncryptFileToBin(const std::string& src_file, const std::string& dst_file);
    MMCV_EXPORT bool EncryptFileToTxt(const std::string& src_file, const std::string& dst_file, const std::string& prefix);
}

#endif
