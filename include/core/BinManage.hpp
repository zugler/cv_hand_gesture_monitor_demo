//  Created by lhd on 2017/10/21.
//  liuhuaida@163.com
//

#ifndef BIN_MANAGE_H
#define BIN_MANAGE_H

#include <iostream>
#include <vector>
#include <array>
#include <core/MMCore.hpp>
#include <core/md5.hpp>

//---------Bin header--------------
//header format
//Magic code(4byte)   MD5(16byte) DATE(16Byte) SDK_Version(4byte)  Model_Version(4byte) Model_NAME(4byte)

//--Magic code
//  0xBEABEFCD

//--MD5
//  magic code and md5 itself are not encoded in md5

//--Date(16byte)
//  from __DATE__  __TIME__


//----------model file name-----------------
//model file name format
//a.b.c.d.e-model
//a: sdk version
//b: date
//c: time
//d: model version
//e: modle bin name

namespace mmcv
{
    //-----global macros----
    const uint32_t MAGIC_CHECK_CODE = 0xBEABEFCD;
    const uint32_t SDK_VERSION = 10;
    const std::array<uint32_t, 1>  supported_model_sdk_version = {{10}};
    
    const uint32_t MAGIC_CODE_BYTE_LENGTH = 4;
    const uint32_t DATE_BYTE_LENGTH = 16;
    const uint32_t MD5_BYTE_LENGTH = 16;
    const uint32_t SDK_VERSION_BYTE_LENGTH = 4;
    const uint32_t MODEL_VERSION_BYTE_LENGTH = 4;
    const uint32_t MODEL_NAME_BYTE_LENGTH = 4;
    const uint32_t HEADER_BYTE_LENGTH =  MAGIC_CODE_BYTE_LENGTH
                                       + DATE_BYTE_LENGTH
                                       + MD5_BYTE_LENGTH
                                       + SDK_VERSION_BYTE_LENGTH
                                       + MODEL_VERSION_BYTE_LENGTH
                                       + MODEL_NAME_BYTE_LENGTH;
    const uint32_t NO_MD5_CHECK_BYTE_LENGTH = MAGIC_CODE_BYTE_LENGTH
                                            + MD5_BYTE_LENGTH;
    
    struct MMCV_EXPORT BinHeader
    {
        uint32_t  magic_code;
        uint8_t   md5[MD5_BYTE_LENGTH];
        uint8_t   date[DATE_BYTE_LENGTH];
        uint32_t  sdk_version;
        uint32_t  model_version;
        uint32_t  model_name;
        
    };

    MMCV_EXPORT bool MagicCodeExist(const std::vector<uint8_t>& buf);
    MMCV_EXPORT bool GenerateMD5Str(const std::vector<uint8_t>& buf, std::string & generated_md5);
    MMCV_EXPORT bool GenerateMD5Vec(const std::vector<uint8_t>& buf, std::vector<uint8_t> & generated_md5);
    MMCV_EXPORT bool GenerateMD5Vec(const uint8_t* buf, size_t buf_len, std::vector<uint8_t> & generated_md5);
    MMCV_EXPORT bool BuildDateStr(std::string & so, bool use_time = true);
    MMCV_EXPORT bool BuildDateVec(std::vector<uint8_t> & vo, bool use_time = true);
    MMCV_EXPORT bool GenerateBinFileName(std::string &bin_file_name, const uint32_t in_model_version = 0, const std::string in_model_bin_name = "");
    MMCV_EXPORT bool CheckMD5(const std::vector<uint8_t>& buf);
    MMCV_EXPORT bool CheckModel(const std::vector<uint8_t>& buf, const uint32_t in_model_version = 0, const uint32_t in_model_name = 0);
    MMCV_EXPORT bool AddHeader(std::vector<uint8_t>& buf, uint32_t in_model_version =0 , uint32_t in_model_num_name =0);
    MMCV_EXPORT bool PrintModelHeader(const std::vector<uint8_t>& buf);
    MMCV_EXPORT bool RemoveHeader(std::vector<uint8_t>& buf);
    MMCV_EXPORT bool CheckModelAndRemoveHeader(std::vector<uint8_t> &model_buf, const uint32_t in_model_version, const uint32_t in_model_name);
    //0: 32bit, 1: 16bit, -1: error
    MMCV_EXPORT int  GetMoldeType(std::vector<uint8_t> &model_buf);

    // 0: mmforward, 1: mace, 2: coreml, -1: error
    MMCV_EXPORT int GetForwardEngineType(std::vector<uint8_t> &model_buf);

}


#endif
