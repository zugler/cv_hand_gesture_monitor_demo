#ifndef MM_CORE_H
#define MM_CORE_H

#include <opencv2/opencv.hpp>
#include <core/MMCommon.hpp>
#include <core/MMDef.hpp>

#define MM_PI CV_PI

namespace mmcv
{

class MMCV_EXPORT Mat : public cv::Mat
{
public:
    Mat()
    {
        colorType = MMCV_UNKNOWN;
    }

    Mat(int rows, int cols, int type, int _colorType) : cv::Mat(rows, cols, type)
    {
        colorType =_colorType;
    }

    Mat(cv::Size size, int type, int _colorType) : cv::Mat(size, type)
    {
        colorType = _colorType;
    }

    Mat(int rows, int cols, int type, int _colorType, const cv::Scalar& s) : cv::Mat(rows, cols, type, s)
    {
        colorType = _colorType;
    }

    Mat(cv::Size size, int type, int _colorType, const cv::Scalar& s) : cv::Mat(size, type, s)
    {
        colorType = _colorType;
    }

    Mat(const cv::Mat& m) : cv::Mat(m)
    {
        colorType = MMCV_UNKNOWN;
    }

    Mat(const cv::Mat& m, int _colorType) : cv::Mat(m)
    {
        colorType = _colorType;
    }

    Mat(int rows, int cols, int type, int _colorType, void* data, size_t step = AUTO_STEP) : cv::Mat(rows, cols, type, data, step)
    {
        colorType = _colorType;
    }

    Mat(cv::Size size, int type, int _colorType, void* data, size_t step = AUTO_STEP) : cv::Mat(size, type, data, step)
    {
        colorType = _colorType;
    }

    void create(int rows, int cols, int type, int _colorType)
    {
        cv::Mat::create(rows, cols, type);
        colorType = _colorType;
    }

    int getFrameRows() const
    {
        if (colorType == MMCV_YV12 || colorType == MMCV_NV12 || colorType == MMCV_YV12 || colorType == MMCV_I420 || colorType == MMCV_NV21)
        {
            return rows / 3 * 2;
        }
        else if (colorType == MMCV_NV12_SEP || colorType == MMCV_NV21_SEP)
        {
            return y_data.rows;
        }
        else
        {
            return rows;
        }
    }

    int getFrameCols() const
    {
        if (colorType == MMCV_NV12_SEP || colorType == MMCV_NV21_SEP)
        {
            return y_data.cols;
        }
        else
        {
            return cols;
        }
    }


    bool empty() const
    {
        
        if (colorType == MMCV_NV12_SEP || colorType == MMCV_NV21_SEP)
        {
            return (y_data.empty() || uv_data.empty());
        }
        else
        {
            return cv::Mat::empty();
        }
    }
    
    Mat clone() const
    {
        if (colorType == MMCV_NV12_SEP || colorType == MMCV_NV21_SEP)
        {
            Mat tmp;
            tmp.y_data = y_data.clone();
            tmp.uv_data = uv_data.clone();
            tmp.colorType = colorType;
            return tmp;
        }
        else
        {
            Mat tmp = cv::Mat::clone();
            tmp.colorType = colorType;
            return tmp;
        }
    }

    Mat operator()(cv::Range _rowRange, cv::Range _colRange) const
    {
        if (colorType == MMCV_NV12_SEP || colorType == MMCV_NV21_SEP)
        {
            Mat tmp;
            tmp.y_data = cv::Mat(y_data, _rowRange, _colRange);
            tmp.uv_data = cv::Mat(uv_data, cv::Range(_rowRange.start / 2, _rowRange.end / 2), cv::Range(_colRange.start / 2, _colRange.end / 2));
            tmp.colorType = colorType;
            return tmp;
        }
        else
        {
            Mat tmp = cv::Mat(*this, _rowRange, _colRange);
            tmp.colorType = colorType;
            return tmp;
        }
    }

    Mat operator()(const cv::Rect& roi) const
    {
        if (colorType == MMCV_NV12_SEP || colorType == MMCV_NV21_SEP)
        {
            Mat tmp;
            tmp.y_data = cv::Mat(y_data, roi);
            tmp.uv_data = cv::Mat(uv_data, cv::Rect(roi.x / 2, roi.y / 2, roi.width / 2, roi.height / 2));
            tmp.colorType = colorType;
            return tmp;
        }
        else
        {
            Mat tmp = cv::Mat(*this, roi);
            tmp.colorType = colorType;
            return tmp;
        }
    }

    cv::Size size() const
    {
        if (colorType == MMCV_NV12_SEP || colorType == MMCV_NV21_SEP)
        {
            return y_data.size();
        }
        else
        {
            return cv::Mat::size();
        }
    }

    int colorType;

    cv::Mat y_data;
    cv::Mat uv_data;

private:
    // disable method
    Mat operator()(const cv::Range* ranges) const;
    Mat operator()(const std::vector<cv::Range>& ranges) const;
};

void MMCV_EXPORT VersionInfo(const std::string& module_name,const std::string& module_compiled_time);

}

void _cv_funcs_();

#endif
