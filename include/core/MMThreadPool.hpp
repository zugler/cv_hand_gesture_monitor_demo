#pragma once

#include <vector>
#include <queue>
#include <thread>
#include <atomic>
#include <condition_variable>
#include <future>
#include <functional>
#include <stdexcept>
#include <core/MMCore.hpp>

namespace mmcv
{

class MMCV_EXPORT ThreadPool
{
    using Task = std::function<void()>;
    // thread pool
    std::vector<std::thread> pool;
    // task queue
    std::queue<Task> tasks;
    // sync
    std::mutex m_lock;
    // condition block
    std::condition_variable cv_task;
    // commit flag
    std::atomic<bool> stoped;
    // idle thread num
    std::atomic<int>  idlThrNum;

public:
    inline ThreadPool(unsigned short size = 4) :stoped{ false }
    {
        idlThrNum = size < 1 ? 1 : size;
        for (size = 0; size < idlThrNum; ++size)
        {
            // init thread
            pool.emplace_back(
                [this]
            {
                // main loop
                while (!this->stoped)
                {
                    std::function<void()> task;
                    {
                        // get task
                        std::unique_lock<std::mutex> lock{ this->m_lock };
                        this->cv_task.wait(lock,
                            [this] {
                                return this->stoped.load() || !this->tasks.empty();
                            }
                        );
                        if (this->stoped && this->tasks.empty())
                            return;
                        task = std::move(this->tasks.front());
                        this->tasks.pop();
                    }
                    idlThrNum--;
                    task();
                    idlThrNum++;
                }
            }
            );
        }
    }
    inline ~ThreadPool()
    {
        stoped.store(true);
        cv_task.notify_all();
        for (std::thread& thread : pool) {
            //thread.detach();
            if (thread.joinable())
                thread.join();
        }
    }

public:
    // commit a task
    // .get() method can be called
    // bind: .commit(std::bind(&Dog::sayHello, &dog));
    // mem_fn: .commit(std::mem_fn(&Dog::sayHello), &dog)
    template<class F, class... Args>
    auto commit(F&& f, Args&&... args) ->std::future<decltype(f(args...))>
    {
        if (stoped.load())
            throw std::runtime_error("commit on ThreadPool is stopped.");

        using RetType = decltype(f(args...));
        auto task = std::make_shared<std::packaged_task<RetType()> >(std::bind(std::forward<F>(f), std::forward<Args>(args)...));
        std::future<RetType> future = task->get_future();
        {
            std::lock_guard<std::mutex> lock{ m_lock };
            tasks.emplace(
                [task]()
                {
                    (*task)();
                }
            );
        }
        cv_task.notify_one();

        return future;
    }

    int idlCount() { return idlThrNum; }

};

}