#ifndef MM_EIGEN_H
#define MM_EIGEN_H

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>

namespace mmcv
{

typedef Eigen::Matrix<float, 2, 4, Eigen::RowMajor> ProjectionMatrixf;
typedef Eigen::Matrix<float, 3, 3, Eigen::RowMajor> RotationMatrixf;
typedef Eigen::Matrix<float, 4, 4, Eigen::RowMajor> ModelMatrixf;

typedef Eigen::Array<float, 1, 2, Eigen::RowMajor> EigenPoint2f;
typedef Eigen::Array<float, 1, 3, Eigen::RowMajor> EigenPoint3f;
typedef Eigen::Matrix<float, 1, 2, Eigen::RowMajor> EigenVec2f;
typedef Eigen::Matrix<float, 1, 3, Eigen::RowMajor> EigenVec3f;
typedef Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> DynamicArrayf;
typedef Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> DynamicMatrixf;

typedef Eigen::Array<double, 1, 2, Eigen::RowMajor> EigenPoint2d;
typedef Eigen::Array<double, 1, 3, Eigen::RowMajor> EigenPoint3d;
typedef Eigen::Matrix<double, 1, 2, Eigen::RowMajor> EigenVec2d;
typedef Eigen::Matrix<double, 1, 3, Eigen::RowMajor> EigenVec3d;
typedef Eigen::Array<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> DynamicArrayd;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> DynamicMatrixd;

template <typename T>
using DynamicArray = Eigen::Array<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

template <typename T>
using DynamicMatrix = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

}

#endif