#ifndef MM_DEF_H
#define MM_DEF_H

#include <vector>

namespace mmcv
{

enum RotationMode
{
    kRotate0    = 0,        // 0 rotation.
    kRotate90   = 90,       // Rotate 90 degrees clockwise.
    kRotate180  = 180,      // Rotate 180 degrees.
    kRotate270  = 270,      // Rotate 270 degrees clockwise.
};

enum ColorType {
    MMCV_UNKNOWN    = 0,            // unknown format
    MMCV_GRAY       = 2,
    MMCV_RGBA       = 4,            // same as Bitmap.Config.ARGB_8888 in android
    MMCV_BGRA       = 5,
    MMCV_I420       = 12,           // YUV_I420 [yyyyuuvv]
    MMCV_NV21       = 17,           // NV21 [yyyyvuvu]
    MMCV_NV12       = 18,           // NV12 [yyyyuvuv]
    MMCV_NV21_SEP   = 19,           // NV21 [yyyyvuvu]
    MMCV_NV12_SEP   = 20,           // NV12 [yyyyuvuv]
    MMCV_BGR        = 24,           // [bgrbgrbgr]
    MMCV_RGB        = 25,           // [rgbrgbrgb]
    MMCV_BGR_PLANAR = 26,           // [rrrbbbggg]
    MMCV_RGB565     = 27,           // same as Bitmap.Config.RGB_565 in android
    MMCV_BGR565     = 28,
    MMCV_YV12       = 842094169,    // YV12 [yyyyvvuu]
};

enum ColorConversion {
    MMCV_BGR2I420       = 0,
    MMCV_BGR2NV12       = 1,
    MMCV_BGR2NV21       = 2,
    MMCV_BGR2BGR_PLANAR = 3,
    MMCV_BGR_PLANAR2BGR = 4,
    MMCV_NV122BGR       = 5,
    MMCV_NV212BGR       = 6,
    MMCV_I4202BGR       = 7,
    MMCV_NV12_SEP2BGR   = 8,
    MMCV_NV21_SEP2BGR   = 9,
};
    
typedef std::vector<std::vector<std::vector<unsigned short> > > D3HALFWEIGHT;
typedef std::vector<std::vector<std::vector<float > > > D3FLOATFWEIGHT;
}



#endif
